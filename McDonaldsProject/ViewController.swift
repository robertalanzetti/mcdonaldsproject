//
//  ViewController.swift
//  McDonaldsProject
//
//  Created by Roberta Lanzetti on 10/12/2019.
//  Copyright © 2019 Roberta Lanzetti. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var bodyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        titleLabel.font = UIFont(name: "Helvetica-Bold", size: 18.0)
        
        bodyLabel.text = "Live at the best McDonald's experience.\nDiscover all of our products made of\n100% Italian meat."
        bodyLabel.font = UIFont(name: "Helvetica", size: 18.0)
    
    }
    
    


}

